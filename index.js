const lowerCase = (word) => {
    return word.toLowerCase()
}

module.exports = { lowerCase }